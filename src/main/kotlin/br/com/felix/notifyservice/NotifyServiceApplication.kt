package br.com.felix.notifyservice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient

@EnableEurekaClient
@SpringBootApplication
class NotifyServiceApplication

fun main(args: Array<String>) {
	runApplication<NotifyServiceApplication>(*args)
}
