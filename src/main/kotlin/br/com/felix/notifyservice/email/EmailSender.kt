package br.com.felix.notifyservice.email

interface EmailSender {

    @Throws(EmailException::class)
    fun send(model: EmailModel)
}