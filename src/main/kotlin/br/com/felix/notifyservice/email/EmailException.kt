package br.com.felix.notifyservice.email

class EmailException(message: String?) : Exception(message)