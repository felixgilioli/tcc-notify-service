package br.com.felix.notifyservice.email

data class EmailModel(
        val para: Set<String>,
        val titulo: String,
        val mensagem: String
)