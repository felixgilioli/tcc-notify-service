package br.com.felix.notifyservice.email

import br.com.felix.notifyservice.NotifyServiceApplication
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.mail.javamail.MimeMessageHelper
import org.springframework.stereotype.Component

@Component
class EmailSenderImpl(private val mailSender: JavaMailSender) : EmailSender {

    override fun send(model: EmailModel) {

        val mimeMessage = mailSender.createMimeMessage()
        val helper = MimeMessageHelper(mimeMessage, false, "UTF-8")
        mimeMessage.setContent(model.mensagem + getTemplate(), "text/html")
        helper.setSubject(model.titulo)
        helper.setTo(model.para.toTypedArray())

        try {
            mailSender.send(mimeMessage)
        } catch (e: Exception) {
            throw EmailException(e.message)
        }
    }

    private fun getTemplate() : String {
        return NotifyServiceApplication::class.java.getResource("/static/assinatura-tcc.html").readText()
    }
}