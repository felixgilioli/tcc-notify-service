package br.com.felix.notifyservice.teste

import br.com.felix.notifyservice.email.EmailModel
import br.com.felix.notifyservice.email.EmailSender
import org.springframework.stereotype.Service

@Service
class TesteServiceImpl(private val emailSender: EmailSender) : TesteService {

    override fun testeEmail() {
        emailSender.send(EmailModel(setOf("felixgilioli1@gmail.com"),
                "teste",
                "testando o teste do testador."))
    }
}
