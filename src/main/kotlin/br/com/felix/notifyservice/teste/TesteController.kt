package br.com.felix.notifyservice.teste

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("teste")
class TesteController(private val testeService: TesteService) {

    @GetMapping("email")
    fun testeEmail() {
        testeService.testeEmail()
    }
}
