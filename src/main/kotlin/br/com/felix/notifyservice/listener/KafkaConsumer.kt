package br.com.felix.notifyservice.listener

import br.com.felix.notifyservice.email.EmailModel
import br.com.felix.notifyservice.email.EmailSender
import com.google.gson.Gson
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.stereotype.Service


@Service
class KafkaConsumer(private val emailSender: EmailSender,
                    private val gson: Gson) {

    @KafkaListener(topics = ["aniversariantes"], groupId = "email")
    fun consume(email: String) {
        emailSender.send(gson.fromJson(email, EmailModel::class.java))
    }
}